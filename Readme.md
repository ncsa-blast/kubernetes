# Blast deployment repo

[Blast](https://blast.readthedocs.io/en/latest/) is a Django web app for the automatic characterization of supernova hosts. The app source code is found at https://github.com/scimma/blast.

This repo serves as the [GitOps](https://www.gitops.tech/) deployment repo for the service hosted at [NCSA](http://ncsa.illinois.edu/). The Helm charts defined here are added as [ArgoCD applications](https://blast.ncsa.illinois.edu/argo-cd) to drive the deployment of Blast services on the Kubernetes cluster.

See individual Helm chart Readme files for deployment-specific documentation.
