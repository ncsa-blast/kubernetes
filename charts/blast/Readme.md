# Blast web app

This Helm chart adapts the Docker Compose deployment found in [the source code repo](https://github.com/scimma/blast) to Kubernetes.
