---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: blast-api
  {{- with .Values.common.annotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  replicas: {{ .Values.web_app.replicas }}
  serviceName: blast-api
  selector:
    matchLabels:
      app: blast-api
  template:
    metadata:
      labels:
        app: blast-api
    spec:
      containers:
      - name: app
        image: {{ .Values.image.url }}
        imagePullPolicy: {{ .Values.image.imagePullPolicy }}
        command:
        - bash
        - entrypoints/docker-entrypoint.app.sh
        env:
          {{- include "blast.env" . | nindent 10 }}
        startupProbe:
          httpGet:
            path: /
            port: {{ .Values.web_app.port }}
            scheme: HTTP
          successThreshold: 1
          periodSeconds: 10
          failureThreshold: 60
          timeoutSeconds: 5
        livenessProbe:
          httpGet:
            path: /acknowledgements
            port: {{ .Values.web_app.port }}
            scheme: HTTP
          timeoutSeconds: 15
          periodSeconds: 30
          failureThreshold: 6
          terminationGracePeriodSeconds: 10
        readinessProbe:
          httpGet:
            path: /acknowledgements
            port: {{ .Values.web_app.port }}
            scheme: HTTP
          timeoutSeconds: 5
          periodSeconds: 10
          failureThreshold: 1
        volumeMounts:
        - name: dustmap
          mountPath: /tmp/.dustmapsrc
          subPath: dustmapsrc
        - name: data
          mountPath: /mnt/data
          subPath: data
        - name: data
          mountPath: /app/static
          subPath: static
      volumes:
        - name: dustmap
          configMap:
            name: blast-config-dustmap
        - name: data
          persistentVolumeClaim:
            claimName: blast-astro-data
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - blast-api
              topologyKey: "kubernetes.io/hostname"
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: celery-worker
  {{- with .Values.common.annotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  replicas: {{ .Values.celery.workers.replicas }}
  serviceName: celery-worker
  selector:
    matchLabels:
      app: celery-worker
  template:
    metadata:
      labels:
        app: celery-worker
    spec:
      containers:
      - name: celery
        image: {{ .Values.image.url }}
        imagePullPolicy: {{ .Values.image.imagePullPolicy }}
        command:
        - bash
        - entrypoints/docker-entrypoint.celery.sh
        env:
          {{- include "blast.env" . | nindent 10 }}
          - name: CELERY_CONCURRENCY
            value: {{ .Values.celery.workers.concurrency | quote }}
        # References for probes:
        #   - https://github.com/celery/celery/issues/4079
        #   - https://medium.com/ambient-innovation/health-checks-for-celery-in-kubernetes-cf3274a3e106
        livenessProbe:
          exec:
            command:
            - sh
            - -c
            - test $(($(date +%s) - $(stat -c %Y /tmp/worker_heartbeat))) -lt 10
          initialDelaySeconds: 10
          periodSeconds: 5
        readinessProbe:
          exec:
            command:
            - sh
            - -c
            - test -e /tmp/worker_ready
          initialDelaySeconds: 10
          periodSeconds: 5
        {{- with  .Values.celery.workers.resources }}
        resources:
          {{- toYaml . | nindent 10 }}
        {{- end }}
        volumeMounts:
        - name: dustmap
          mountPath: /tmp/.dustmapsrc
          subPath: dustmapsrc
        - name: data
          mountPath: /mnt/data
          subPath: data
      volumes:
        - name: dustmap
          configMap:
            name: blast-config-dustmap
        - name: data
          persistentVolumeClaim:
            claimName: blast-astro-data
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - celery-worker
              topologyKey: "kubernetes.io/hostname"
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: celery-worker-sed
  {{- with .Values.common.annotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  replicas: {{ .Values.celery.sed_workers.replicas }}
  serviceName: celery-worker-sed
  selector:
    matchLabels:
      app: celery-worker-sed
  template:
    metadata:
      labels:
        app: celery-worker-sed
    spec:
      containers:
      - name: celery
        image: {{ .Values.image.url }}
        imagePullPolicy: {{ .Values.image.imagePullPolicy }}
        command:
        - bash
        - entrypoints/docker-entrypoint.celery.sh
        env:
          {{- include "blast.env" . | nindent 10 }}
          - name: CELERY_QUEUES
            value: "sed,celery"
          - name: CELERY_CONCURRENCY
            value: {{ .Values.celery.sed_workers.concurrency | quote }}
        livenessProbe:
          exec:
            command:
            - sh
            - -c
            - test $(($(date +%s) - $(stat -c %Y /tmp/worker_heartbeat))) -lt 10
          initialDelaySeconds: 10
          periodSeconds: 5
        readinessProbe:
          exec:
            command:
            - sh
            - -c
            - test -e /tmp/worker_ready
          initialDelaySeconds: 10
          periodSeconds: 5
        {{- with  .Values.celery.sed_workers.resources }}
        resources:
          {{- toYaml . | nindent 10 }}
        {{- end }}
        volumeMounts:
        - name: dustmap
          mountPath: /tmp/.dustmapsrc
          subPath: dustmapsrc
        - name: data
          mountPath: /mnt/data
          subPath: data
      volumes:
        - name: dustmap
          configMap:
            name: blast-config-dustmap
        - name: data
          persistentVolumeClaim:
            claimName: blast-astro-data
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - celery-worker-sed
              topologyKey: "kubernetes.io/hostname"
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: celery-beat
  {{- with .Values.common.annotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  replicas: {{ .Values.celery.beat.replicas }}
  serviceName: celery-beat
  selector:
    matchLabels:
      app: celery-beat
  template:
    metadata:
      labels:
        app: celery-beat
    spec:
      containers:
      - name: celery-beat
        image: {{ .Values.image.url }}
        imagePullPolicy: {{ .Values.image.imagePullPolicy }}
        command:
        - bash
        - entrypoints/docker-entrypoint.celery_beat.sh
        env:
          {{- include "blast.env" . | nindent 10 }}
        volumeMounts:
        - name: data
          mountPath: /mnt/data
          subPath: data
      volumes:
        - name: data
          persistentVolumeClaim:
            claimName: blast-astro-data
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - celery-beat
              topologyKey: "kubernetes.io/hostname"
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: celery-flower
  {{- with .Values.common.annotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  replicas: {{ .Values.celery.flower.replicas }}
  serviceName: celery-flower
  selector:
    matchLabels:
      app: celery-flower
  template:
    metadata:
      labels:
        app: celery-flower
    spec:
      containers:
      - name: flower
        image: {{ .Values.image.url }}
        imagePullPolicy: {{ .Values.image.imagePullPolicy }}
        command:
        - bash
        - entrypoints/docker-entrypoint.flower.sh
        env:
          {{- include "blast.env" . | nindent 10 }}
